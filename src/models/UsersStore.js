import { observable } from "mobx";

export default class UsersStore {
  @observable activeUsers = [];
  @observable showEditModal = false;
  @observable deletedUsers = [];
  @observable selectedUser = [];
  @observable errorMessage = '';

  constructor() {
    this.activeUsers = [
      { id: 1, name: 'Richa', mobile: '9999999', email: 'abc@mail.com'},
      { id: 2, name: 'Jalpesh', mobile: '9999999', email: 'abcde@mail.com'},
      { id: 3, name: 'Poonam', mobile: '9999999', email: 'abcd@mail.com'}  
    ];
  }

  onClickAddUser = () => {
    this.showEditModal = true;
    this.errorMessage = '';
    this.selectedUser = { id: 0, name: '', mobile: '', email: ''}
  }

  onClickEditUser = (id) => {
    this.selectedUser = Object.assign({}, this.activeUsers.find((a) => a.id === id));
    this.errorMessage = '';
    this.showEditModal = true;
  }

  onClickDelete = (id) => {
    let userToBeDeleted = this.activeUsers.find((a) => a.id === id);
    this.deletedUsers.push(userToBeDeleted);
    this.activeUsers = this.activeUsers.filter((a) => a.id !== id);
  }

  onClickRestore = (id) => {
    let userToBeRestored = this.deletedUsers.find((a) => a.id === id);
    this.activeUsers.push(userToBeRestored);
    this.deletedUsers = this.deletedUsers.filter((a) => a.id !== id);
  }

  

  isValidEmail = () => {
    let allUserEmails = [];
    this.activeUsers.map((a) => {
      if(a.id !== this.selectedUser.id){
        allUserEmails.push(a.email);
      }
    });
    this.deletedUsers.map((d) => allUserEmails.push(d.email));
    let matchExists = allUserEmails.includes(this.selectedUser.email);
    if(matchExists){
      this.errorMessage = "Enter unique email"
      return false;
    }
    return true;
    
  }


  onSubmitForm = () => {
    if(this.isValidEmail()){
      if(this.selectedUser.id){
        let editUser = this.activeUsers.find((a) => a.id === this.selectedUser.id)
        editUser.name = this.selectedUser.name;
        editUser.mobile = this.selectedUser.mobile;
        editUser.email = this.selectedUser.email;
      }
      else{
        let newId = Math.random(1, 100);
        this.selectedUser.id = newId;
        this.activeUsers.push(this.selectedUser);
      }    
      this.showEditModal = false;
    }
  }    
}
