import React from "react";
import { render } from "react-dom";
import DevTools from "mobx-react-devtools";
import App from "./App";
import UsersStore from './models/UsersStore';

const userStore = new UsersStore();

render(
  <div>
    <DevTools />
    <App store={userStore} />
  </div>,
  document.getElementById("root")
);
