import React from "react";
import { observer } from "mobx-react";
import { Modal, Button } from "react-bootstrap";

@observer
class AddEditUsersModal extends React.Component {

    constructor(props){
        super(props);
    }

  render() {
      const {store} = this.props;
    return (
        <div>
            <Modal show={store.showEditModal} onHide={() => {store.showEditModal = false}}>
                <Modal.Header closeButton>
                <Modal.Title>{store.selectedUser.id ? 'Add User' : 'Edit User'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p style={{color: 'red'}}>{store.errorMessage}</p>
                    <label>Name</label>
                    <input name="name" label="Name" value={store.selectedUser.name} onChange={(e) => store.selectedUser.name = e.target.value} />
                    <br/><label>Mobile</label>
                    <input name="mobile" label="Mobile" value={store.selectedUser.mobile} onChange={(e) => store.selectedUser.mobile = e.target.value} />
                    <br/><label>Email</label>
                    <input name="email" label="Email" value={store.selectedUser.email} onChange={(e) => store.selectedUser.email = e.target.value} />
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={() => {store.showEditModal = false}}> 
                    Close
                </Button>
                <Button variant="primary" onClick={store.onSubmitForm}>
                    Save Changes
                </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
  }
}

export default AddEditUsersModal;
