import React, { Component } from "react";
import { observer } from "mobx-react";
import { Table, Button } from "react-bootstrap";
import AddEditUsersModal from "./AddEditUsersModal";

@observer
class Users extends React.Component {

    constructor(props){
        super(props);
    }

  render() {
      const {store} = this.props;
    return (
        <div>
            <Button onClick={store.onClickAddUser}>Add New User</Button>
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>                    
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {store.activeUsers.length ? store.activeUsers.map((a, k) => 
                        <tr key={k}>
                            <td>{k + 1}</td>
                            <td>{a.name}</td>
                            <td>{a.mobile}</td>
                            <td>{a.email}</td>
                            <td><Button onClick={() => store.onClickEditUser(a.id)}>Edit</Button>  <Button onClick={() => store.onClickDelete(a.id)}>Delete</Button></td>
                        </tr>
                    ): null
                    }                    
                </tbody>
            </Table>
            <AddEditUsersModal store={store} />
        </div>
    );
  }
}

export default Users;
