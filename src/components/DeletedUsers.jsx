import React, { Component } from "react";
import { observer } from "mobx-react";
import { Table, Button } from "react-bootstrap";

@observer
class DeletedUsers extends React.Component {

    constructor(props){
        super(props);
    }

  render() {
      const {store} = this.props;
    return (
        <div>
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>                    
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {store.deletedUsers.length ? store.deletedUsers.map((a, k) => 
                        <tr key={k}>
                            <td>{k + 1}</td>
                            <td>{a.name}</td>
                            <td>{a.mobile}</td>
                            <td>{a.email}</td>
                            <td><Button onClick={() => store.onClickRestore(a.id)}>Restore</Button></td>
                        </tr>
                    ): null
                    }
                    
                </tbody>
            </Table>
        </div>
    );
  }
}

export default DeletedUsers;
