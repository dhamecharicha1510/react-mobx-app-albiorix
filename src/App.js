import React, { Component } from "react";
import { observer } from "mobx-react";
import { Tabs, Tab } from "react-bootstrap";
import Users from './components/Users';
import DeletedUsers from './components/DeletedUsers';



@observer
class App extends React.Component {
    constructor(props){
        super(props);
        this.state={
            activeKey: 'users'
        }
        this.handleSelect = this.handleSelect.bind(this);
    }

    handleSelect(k){
        this.setState({
            activeKey: k
        });
    }

  render() {
    return (
        <Tabs defaultActiveKey="users" id="uncontrolled-tab-example" activeKey={this.state.activeKey} onSelect={this.handleSelect}>
            <Tab eventKey="users" title="Users">
                <Users store={this.props.store} />
            </Tab>
            <Tab eventKey="deletedUsers" title="DeletedUsers">
                <DeletedUsers store={this.props.store} />
            </Tab>
        </Tabs>
    );
  }
}

export default App;
